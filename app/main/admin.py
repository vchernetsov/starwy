from django.contrib import admin
from .models import Character


@admin.register(Character)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('file_name', 'created_at')
    search_fields = ('file_name', 'created_at')
