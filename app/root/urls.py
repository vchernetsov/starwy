from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from main.urls import urlpatterns as main_urlpatterns


urlpatterns = []
urlpatterns += main_urlpatterns
urlpatterns.append(path('admin/', admin.site.urls))

# Static endpoints
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
