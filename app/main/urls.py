"""MAin application urls module"""

from django.urls import path

from main import views


urlpatterns = [
    path('', views.index, name='index'),
    path('<int:page>/', views.index, name='main-index-paged'),
    path('characters/<str:collection>/<int:page>/', views.characters, name='main-characters'),
    path('explore/<str:collection>/<int:page>/', views.aggregated, name='main-explore'),
    path('about/', views.about, name='main-about'),
]
