from django.db import models


class Character(models.Model):
    file_name = models.CharField(max_length=1024)
    upload = models.FileField(upload_to='characters')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return self.file_name.rsplit('.', 1)[0]
