"""Gunicorn configuration."""
import sys

from envparse import env

sys.path.append('/usr/src/app/coffee/')
workers = 8
if 'development' in env.str('ENVIRONMENT'):
    reload = True
    workers = 2
address = env.str('BIND_ADDRESS')
port = env.str('BIND_PORT')
bind = "%s:%s" % (address, port)
