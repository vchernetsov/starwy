"""Main application views module."""

from collections import OrderedDict
import petl

from django.shortcuts import render
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, get_object_or_404

from main.models import Character
from .etl import Collection

PER_PAGE = 10

@login_required
def index(request, page=1):
    """View function returns a list of datasets available."""
    if request.method == 'POST':
        collection = Collection()
        collection.fetch()
        collection.transform()
        collection.save()
        return redirect('/')
    paginator = Paginator(Character.objects.all(), PER_PAGE)
    context = {
        "current": "",
        "page": paginator.get_page(page),
        "paginator": paginator,
    }
    return render(template_name="index.html", request=request, context=context)

@login_required
def characters(request, collection, page=1):
    """Function lets user to be able to inspect all previously downloaded
    datasets, as well as do simple exploratory operations on it."""

    entry = get_object_or_404(Character, pk=collection)
    data = petl.fromcsv(entry.upload.path)
    headers = ['name', 'height', 'mass', 'hair_color', 'skin_color',
               'eye_color', 'birth_year', 'gender', 'homeworld']
    short = petl.cut(data, *headers)
    paginator = Paginator(list(short[1:]), PER_PAGE)
    context = {
        "uri_prefix": "characters",
        "collection": entry,
        "page": paginator.get_page(page),
        "paginator": paginator,
        "headers": headers,
    }
    return render(template_name="characters.html", request=request, context=context)

@login_required
def aggregated(request, collection, page=1):
    """Function returns a list of aggregated count of data lines."""
    entry = get_object_or_404(Character, pk=collection)
    clicked = request.GET.get('clicked')
    params = request.GET.get('params') or ''
    params = params.split(',')
    headers = ['birth_year', 'homeworld', 'name', 'height', 'mass', 'hair_color', 'skin_color', 'eye_color', 'gender', 'date']
    for param in params:
        if param not in headers or not param:
            params.remove(param)
    if clicked in headers:
        if clicked in params:
            params.remove(clicked)
        else:
            params.append(clicked)
        clicked = None
    params_string = ",".join(params)
    data = petl.fromcsv(entry.upload.path)
    if not params:
        params = headers
    aggreg = OrderedDict()
    aggreg['count'] = len
    data = petl.convert(data, {x: lambda x: (x, ) for x in headers})
    short = petl.aggregate(data, key=params, aggregation=aggreg)
    paginator = Paginator(list(short[1:]), PER_PAGE)
    context = {
        "uri_prefix": "explore",
        "collection": entry,
        "page": paginator.get_page(page),
        "paginator": paginator,
        "headers": headers,
        "params": params + ['count', ],
        "params_string": params_string,
    }
    return render(template_name="explore.html", request=request, context=context)

@login_required
def about(request):
    """This is a small page about me."""
    return render(template_name="about.html", request=request)
