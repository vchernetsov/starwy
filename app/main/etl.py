"""Simple data ETL module"""
import datetime
import tempfile
from dateutil import parser
import requests
import petl

from main import models


class FetchingError(Exception):
    def __init__(self, response):
        self.message = f"Got error {response.status_code} when fetching the data!"


class Collection(object):

    url = "https://swapi.dev/api/people/"
    planets_url = "https://swapi.dev/api/planets/"

    def __init__(self):
        """Class constructor. Initialises instance with file name and empty data list."""
        self.__cache = {}
        self.dataset = []

    @staticmethod
    def get_filename(dt=None):
        """Method returns a file name for collection"""
        file_name = dt.strftime("%b. %d, %Y, %H:%M %p.csv").replace("PM", "p.m.").replace("AM", "a.m.")
        return file_name

    def _fetch(self, url):
        """Method performs a cached HTTP call to `url`. If `cache`==True eneables internal HTTP call caching mechanics.
        """
        if url in self.__cache.keys():
            return self.__cache[url]
        response = requests.get(url=url)
        if response.status_code != 200:
            raise FetchingError(response)
        data = response.json()
        self.__cache[url] = data
        return data

    def pages(self, url):
        """Method-generator returns a data portions fetched from remote django paged API."""
        page = self._fetch(url)
        data = page['results']
        yield data
        next_url = page['next']
        while next_url:
            paged_data = self._fetch(next_url)
            next_url = paged_data['next']
            yield paged_data['results']

    def fetch(self):
        """Method fetches a dataset placen in self.cha`"""
        data = []
        for page in self.pages(self.url):
            data += page
        self.dataset = petl.fromdicts(data)

    def planets(self):
        """Method returns a dict of planet names with url as a key."""
        data = []
        for page in self.pages(self.planets_url):
            data += page
        planet_names = {}
        for x in data:
            key = x['url']
            value = x['name']
            planet_names[key] = value
        return planet_names

    def transform(self):
        """Method transforms data accordingly to the task."""
        planets = self.planets()
        # Resolve the ​ homeworld​ field into the homeworld's name (​ /planets/1/ -> Tatooine )
        self.dataset = petl.convert(self.dataset, {'homeworld': lambda x: planets[x]})
        # Add a ​ date​ column (​ %Y-%m-%d​ ) based on edited​ date

        def add_date(x):
            edited = parser.parse(x.get('edited'))
            return edited.strftime("%Y-%m-%d")

        self.dataset = petl.addfield(self.dataset, 'date', add_date)

    def save(self):
        """Save CSV file and character entry."""
        now = datetime.datetime.now()
        file_name = self.get_filename(now)
        character = models.Character()
        character.file_name = file_name
        fh = tempfile.NamedTemporaryFile()
        character.upload.save(file_name, content=fh)
        character.save()
        petl.tocsv(self.dataset, character.upload)
        fh.close()
