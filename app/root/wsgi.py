"""
WSGI config for b project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
from envparse import env
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'root.settings')

application = get_wsgi_application()

DEBUG = env.bool('DEBUG')
if DEBUG:
    # apply Werkzeug debugger as WSGI debugging application
    from werkzeug.debug import DebuggedApplication
    application = DebuggedApplication(application, True)
